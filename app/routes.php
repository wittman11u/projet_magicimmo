<?php

$app->get('/', function () use ($app) {
    $app->render('home.twig');
});

$app->get('/recherche', function () use ($app) {
    if (!empty($_GET)) {
        $annonces = \app\models\Annonce::searchAnnonces($_GET);
    } else {
        $annonces = "";
    }
    $app->render('recherche.twig', array('annonces' => $annonces));
});

$app->get('/ajout', function () use ($app) {
    $app->render('ajout.twig');
});

$app->get('/annonce:id', function ($id) use ($app) {
    $annonce = \app\models\Annonce::findAnnonce($id);
    $app->render('annonce.twig', array('annonce' => $annonce));
});

$app->get('/mail:id', function ($id) use ($app) {
    $annonce = \app\models\Annonce::findAnnonce($id);
    $app->render('contact.twig', array('annonce' => $annonce));
});

$app->post('/insertion', function () use ($app) {
    $idAnnonce = \app\models\Annonce::addAnnonce($_POST);
    $app->redirect('annonce' . $idAnnonce);
});
?>