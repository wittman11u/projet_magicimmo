<?php

namespace app;

use Illuminate\Database\Capsule\Manager as DB;

class DbConnect {
	public static function EloConfigure($filename) {
		$config = parse_ini_file ( $filename );

		if (! $config)
			throw new \Exception ( "Could not parse file $filename" );
		$capsule = new DB ();
		$capsule->addConnection ( $config );
		$capsule->setAsGlobal ();
		$capsule->bootEloquent ();
	}
}

?>
