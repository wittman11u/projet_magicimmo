<?php
namespace app\models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent
{
    protected $table = 'users';
    protected $key = 'id';
    protected $timestamp = false;

    public static function isAdmin($pass)
    {
        $user = User::where('login', 'like', 'admin');
        if (password_verify($pass, $user->password)) {
            return true;
        } else {
            return false;
        }
    }

}

?>
