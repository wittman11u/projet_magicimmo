<?php
namespace app\models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Annonce extends Eloquent
{
    protected $table = 'annonce';
    protected $key = 'id';
    protected $timestamp = false;

    public static function addAnnonce($donnees)
    {
        $annonce = new \app\models\Annonce();
        $annonce->titre = $donnees['titre'];
        $annonce->ville = strtolower($donnees['ville']);
        $annonce->quartier = strtolower($donnees['quartier']);
        $annonce->superficie = $donnees['superficie'];
        $annonce->typeDeBien = $donnees['typeBien'];
        $annonce->typeAnnonce = $donnees['type'];
        $annonce->nbPiece = $donnees['nbPieces'];
        $annonce->description = $donnees['description'];
        $annonce->prix = $donnees['prix'];
        $annonce->email = $donnees['mail'];
        $annonce->tel = $donnees['tel'];
        $annonce->password = password_hash($donnees['pass'], PASSWORD_DEFAULT);
        $annonce->save();
        return $annonce->id;
    }

    public static function findAnnonce($id)
    {
        return \app\models\Annonce::find($id);
    }

    public static function searchAnnonces($criteres)
    {

        if (isset ($criteres['keyWord'])) {
            $requete = \app\models\Annonce::where('titre', 'like', $criteres['keyWord'] . '%');
        }

        if (!empty  ($criteres['ville'])) {
            $requete->where('ville', 'like', $criteres['ville'] . '%');
        }
        if (!empty  ($criteres['quartier'])) {
            $requete->where('quartier', 'like', $criteres['quartier'] . '%');
        }

        if (!empty  ($criteres['superficieMin'])) {
            $requete->where('superficie', '>=', $criteres['superficieMin']);
        }

        if (!empty  ($criteres['superficieMax'])) {
            $requete->where('superficie', '<=', $criteres['superficieMax']);
        }

        if (!empty  ($criteres['typeBien'])) {
            $requete->where('typeDeBien', 'like', $criteres['typeBien']);
        }

        if (!empty ($criteres['type'])) {
            $requete->where('typeAnnonce', 'like', $criteres['type']);
        }

        if (!empty  ($criteres['nbPieces'])) {
            $requete->where('nbPiece', '=', $criteres['nbPieces']);
        }

        if (!empty  ($criteres['prixMin'])) {
            $requete->where('prix', '>=', $criteres['prixMin']);
        }

        if (!empty  ($criteres['prixMax'])) {
            $requete->where('prix', '<=', $criteres['prixMax']);
        }

        return $requete->orderBy('created_at', 'desc')->get()->toArray();
    }


}

?>
