<?php
require_once '../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use app\DbConnect;

DbConnect::EloConfigure ( '../dbconfig.ini' );
$app=new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig(),
    'templates.path' => '../app/templates'
));

require '../app/routes.php';

$app->run();
?>